#!/bin/bash

function usage
{
    echo Usage:
    echo "  setup.sh tipo for setting tipo"
    echo "  setup.sh miro for setting miro"
    exit 0       
}

echo 
echo 
echo "*********************************************"
echo "         CWM for Xperia Tipo/Miro            "
echo "*********************************************"
echo 
echo 

MODEL=$1

CWMDIR=$PWD/bootable/recovery
FILESDIR=$CWMDIR/files
TIPOBOOT=$FILESDIR/boot_tipo.img
MIROBOOT=$FILESDIR/boot_miro.img
BUILDDIR=$FILESDIR/recoverybuild.sh

PATH=$PATH:$PWD/out/host/linux-x86/bin

if [ $MODEL == "tipo" ]; then
	echo "Setting up cwm for tipo"
	$PWD/build/tools/device/mkvendor.sh Sony ST21i $TIPOBOOT
	cp $FILESDIR/tipo/* device/Sony/ST21i
	echo "Done"
	echo
	echo "Now run recoverybuild.sh tipo"
fi

if [ $MODEL == "miro" ]; then
	echo "miro"
	$PWD/build/tools/device/mkvendor.sh Sony ST23i $MIROBOOT
	cp $FILESDIR/miro/* device/Sony/ST23i
	echo "Done"
	echo
	echo "Now run recoverybuild.sh miro"
fi

cp $BUILDDIR .
chmod +x recoverybuild.sh

