#!/bin/bash

function usage
{
    echo Usage:
    echo "  setup.sh tipo for setting tipo"
    echo "  setup.sh miro for setting miro"
    exit 0       
}

echo 
echo 
echo "***********************************************"
echo "          CWM for xperia tipo/miro             "
echo "***********************************************"
echo 
echo 

MODEL=$1

FILESDIR=$PWD/bootable/recovery/files

if [ $MODEL == "tipo" ]; then
	echo 
	echo 
	echo "Building CWM for tipo"
	echo 
	echo 
	. build/envsetup.sh
	lunch full_ST21i-eng
	make -j2 recoveryimage
	mkdir offline_recovery
	mkdir offline_recovery/ST21i
	cp -r $FILESDIR/recovery/* ./offline_recovery/ST21i/
	cd out/target/product/ST21i/recovery/root
	tar -cvf /home/eclipse/CyanogenModICS/offline_recovery/ST21i/files/recovery.tar *
	cd ../../../../../../	
	echo "Done."
	echo
	echo "Now go to $PWD/offline_recovery/ST21i and use install.sh"
	echo "to install the cwm to tipo"
fi

if [ $MODEL == "miro" ]; then
	echo "Building CWM for miro"
	. build/envsetup.sh
	lunch full_ST23i-eng
	make -j2 recoveryimage
	mkdir offline_recovery
	mkdir offline_recovery/ST23i
	cp -r $FILESDIR/recovery/* ./offline_recovery/ST23i/
	cd out/target/product/ST23i/recovery/root
	tar -cvf /home/eclipse/CyanogenModICS/offline_recovery/ST23i/files/recovery.tar *
	cd ../../../../../../	
	echo "Done."
	echo
	echo "Now go to $PWD/offline_recovery/ST23i and use install.sh"
	echo "to install the cwm to miro"
fi

